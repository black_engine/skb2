/**
 * Created by whiterblack on 24.11.16.
 */
var User = require('./models/User').User;
var Pet = require('./models/Pet').Pet;
var async = require('async');
var request = require('request');

async.series([
    function (cb) {
        async.parallel([
            function (cb) {
                Pet.base.connection.once('open', cb);
            },
            function (cb) {
                User.base.connection.once('open', cb);
            }
        ],cb)
    },
    function (cb) {
        async.parallel([
            function (cb) {
                User.remove({}, cb);
            },
            function (cb) {
                Pet.remove({}, cb);
            }
        ],cb);
    },
    function (cb) {
        async.waterfall([
          function (cb) {
              request('https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json',{json:true},cb)
          },
            function (req,data,cb) {
                async.parallel([
                    function (cb) {
                        async.forEach(data.pets,function (pet,cb) {
                            var _pet = new Pet(pet);
                            _pet.save(cb);
                        },cb);
                    },
                    function (cb) {
                        async.forEach(data.users,function (pet,cb) {
                            var _user = new User(pet);
                            _user.save(cb);
                        },cb);
                    }
                ],cb);
            }
        ],cb)
    }
],function () {
    console.log(arguments);
})