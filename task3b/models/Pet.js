/**
 * Created by whiterblack on 24.11.16.
 */
var mongoose = require('../lib/mongoose');
var Schema = mongoose.Schema;
var schema = new Schema({
    id:String,
    userId:String,
    type:String,
    color:String,
    age:Number
});
exports.Pet = mongoose.model('Pet',schema);